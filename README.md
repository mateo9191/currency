# Currency Conventer

## Spis treści:
1. Branch master
2. Wymagane oprogramowanie


## Branch master:
### Opis:
kalkulator walut pobierający aktualne kursy z http://api.fixer.io/latest co 12 godzin zaczynajac od 8:00

### Linki:
* [User Guide](https://bitbucket.org/mgoniprowski/currency/src/293e59508c79bc2e88ffdf32b9d306b30a1c4162/docs/?at=master)
* [Aplikacja](https://bitbucket.org/mgoniprowski/currency/src/293e59508c79bc2e88ffdf32b9d306b30a1c4162/docs/?at=master)

## Wymagane oprogramowanie:
* Dowolne urządzenie z systemem Android minimum 4.0.3 

## Instalacja programu:
Aby zacząć używać programu należy skopiować plik app-debug.apk oraz włączyć debugowanie USB na urządzeniu